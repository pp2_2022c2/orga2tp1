;%include "io.inc"
global _agregar_abb
extern malloc
extern _printf

section .data
    formato db "La posicion es: %f", 10,13,0

section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging

    push 5
    jmp _agregar_abb
    ;write your code here
    xor eax, eax
    ret
    
_agregar_abb:
    push ebp        ;me guardo el valor de BasePointer
    mov ebp,esp     ;mi nuevo basepoint es esp
    
    ;mov edx, [ebp + 12] ;esto es para un segundo parametro
    mov ebx, [ebp + 8]      ;este es el paramtro que viene desde c , simulado en ln 10
    ;cmp ebx, 0      ;Si es cero es por que no hay nodo
    jmp agregoNodo
    
agregoNodo:
    push ebx        ;meto en la pila valor
    push 12         ;para que el malloc sepa que tiene que guardar 12 bytes
    call malloc
    add esp,4       ;descarto el ultimo valor de la pila (12)
    pop ebx         ;saco el tope de la pila y lo guardo en ebx
    
    ;en eax esta la posicion del bloque de memoria reservado por malloc
    ;[eax] guardamos elvalor del nodo 
    ;[eax + 4] nodo izq [eax + 8] nodo der (ambos son posiciones de memorio)
    mov [eax], ebx  ;guardamos el valor de ebx en la posicion de memoria que esta guardad en eax. EL malloc la puso ahi
    mov ebx, 0
    mov [eax + 4], ebx
    mov [eax + 8], ebx
    
    mov [ebp + 8], eax  ;esto guarda la posicion de memoria del inicio del nodo en ebp+8 que es el parametro de entrada
    ;me parece que aca estoy perdiendo el valor ebp que tenia guardado en la pila. Por eso le puse ebp+8
    jmp finalizar
    
finalizar:
    mov esp,ebp     ;apuntamos la pila a donde habiamos hecho la falsa base
    pop ebp         ;suma el esp y saca el el ebp que habiamos guardado una posicion abajo de la falsa base y lo asigna a ebp
    ;basicamente devuelve el valor original de base
    ;para este pop tengo que estar parado en el valor ebp guardado al inicio
    
;    push 23232382
;   push formato
;    call printf
;    add esp,4
    
    mov eax, 1
    mov ebx, 0
    int 0x80